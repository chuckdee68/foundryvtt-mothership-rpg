/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class SimpleActorSheet extends ActorSheet {

  /** @override */
	static get defaultOptions() {
	  return mergeObject(super.defaultOptions, {
  	  classes: ["worldbuilding", "sheet", "actor"],
  	  template: "systems/mothership/templates/actor-sheet.html",
      width: 600,
      height: 600,
        scrollY: [
          ".weapons .weapons-list"
        ],
      tabs: [{navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "description"}]
    });
  }

  /* -------------------------------------------- */

  /** @override */
  getData() {
    const data = super.getData();
    data.dtypes = ["String", "Number", "Boolean"];
    for ( let attr of Object.values(data.data.attributes) ) {
      attr.isCheckbox = attr.dtype === "Boolean";
    }

    data.actor = duplicate(this.actor.data);
    data.items = this.actor.items.map(i => {
      i.data.labels = i.labels;
      return i.data;
    });
    data.items.sort((a, b) => (a.sort || 0) - (b.sort || 0));

    data.itemsByType = {};
    for (const item of data.items) {
      let list = data.itemsByType[item.type];
      if (!list) {
        list = [];
        data.itemsByType[item.type] = list;
      }
      list.push(item);
    }

    const locations = {
      "righthand" :  {key: "righthand", name: "Right Hand", items: []},
      "lefthand" : {key: "lefthand", name: "Left Hand", items: []},
      "belt1" :  {key: "belt1", name: "Belt1", items: []},
      "belt2" : {key: "belt2", name: "Belt2", items: []},
      //"backpack" : {key: "backpack", name: "Backpack", items: []}
    };

    for(const item of data.items){
      if(item.data.location){
        if(item.data.location === "backpack"){
          //locations[item.data.location].items.push(item);
        }else{
          locations[item.data.location].items.push(item);
        }
      }else{
        item.data.location = "backpack";
        this.actor.updateEmbeddedEntity('OwnedItem', item);
        //locations["backpack"].items.push(item); // <--- ERROR HERE
      }
    }

    const isTwoHanded = (locations["lefthand"].items.length === 0 || locations["righthand"].items.length === 0 );




    //data.data.isTwoHanded = isTwoHanded;
   // data.isTwoHanded = isTwoHanded;
    this.actor.update({ 'data.isTwoHanded': isTwoHanded });
    data.data.owned.gear = this._checkNull(data.itemsByType['gear']);
    data.data.owned.weapons = this._checkNull(data.itemsByType['weapon']);
    data.data.owned.advancements = this._checkNull(data.itemsByType['advancement']);
    data.data.owned.abilities = this._checkNull(data.itemsByType['ability']);
    data.data.owned.injuries = this._checkNull(data.itemsByType['injury']);
    data.data.owned.afflictions = this._checkNull(data.itemsByType['affliction']);
    data.data.owned.treasures = this._checkNull(data.itemsByType['treasure']);

    data.data.owned.inventory = [
      ...data.data.owned.gear

    ];

    data.data.owned.consequences = [
      ...data.data.owned.afflictions,
      ...data.data.owned.injuries
    ];

    mergeObject(data, {locations});

   // console.log(data);
    return data;
  }

  /* -------------------------------------------- */

  /** @override */
	activateListeners(html) {


    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Update Inventory Item
    html.find('.item-edit').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.getOwnedItem(li.data("itemId"));
      item.sheet.render(true);
    });

    // Delete Inventory Item
    html.find('.item-delete').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      this.actor.deleteOwnedItem(li.data("itemId"));
      li.slideUp(200, () => this.render(false));
    });

    html.find('.gear-create').click(e => {
      this.actor.createEmbeddedEntity("OwnedItem", {name : "New Gear", type: "gear"})
    });

    html.find('.weapon-create').click( e=> {
      this.actor.createEmbeddedEntity("OwnedItem", {name : "New Weapon", type: "weapon"})
    });

    html.find('.treasure-create').click( e=> {
      this.actor.createEmbeddedEntity("OwnedItem", {name : "New Treasure", type: "treasure"})
    });


    html.find('.archtype').change( e=> {
      console.log( e.target.value);
    });
    // Add or Remove Attribute
    html.find(".attributes").on("click", ".attribute-control", this._onClickAttributeControl.bind(this));

    html.find('.rollable').click(this._onStatRoll.bind(this));

    html.find('.rollable-weapon').click(this._onWeaponRoll.bind(this));

    html.find('.rollable-check').click(this._onCheckRoll.bind(this));


    if ( this.actor.owner ) {
      // Item Dragging
      let handler = ev => this._onDragItemStart(ev);
      html.find('li.item').each((i, li) => {
        if ( li.classList.contains("inventory-header") ) return;
        li.setAttribute("draggable", true);
        li.addEventListener("dragstart", handler, false);
      });
    }

    super.activateListeners(html);

  }

  /** @override */
  async _onDrop (event) {
    event.preventDefault();

    const dropSlotType = $(event.target).parents('.location').attr('data-location');
    const dropSlotItems = $(event.target).parents('.location').find('.item');
    //const dropContainerType = $(event.target).parents('.item-container').attr('data-location'); // if the drop target is of type spellSlot then check if the item dragged onto it is a spell.

    //console.log("DropSlot: " + dropSlotType);
    //console.log("Equipped");

    const dragData = event.dataTransfer.getData('text/plain');
    const dragItem = JSON.parse(dragData);
    //console.log(dragData);

    if (dragItem && dragItem.data && ( dragItem.data.type === 'weapon' || dragItem.data.type === "gear" )) {
      //if (dragItem.actorId === this.actor._id) return false;
      if(dragItem.data.data.equippable){

        // Are we equipping or unequipping?
        if(dragItem.data.data.location === "backpack"){
          //Put the item in a slot
          if(  dropSlotItems.length > 0){
            // Remove the existing item being held/stored
            const removeID =  $(dropSlotItems[0]).attr('data-item-id');
            await this.actor.updateEmbeddedEntity("OwnedItem", {_id: removeID, "data.location": "backpack"});
          }
          dragItem.data.data.location = dropSlotType;
          dragItem.data.data.equipped = true;
          await this.actor.updateEmbeddedEntity('OwnedItem', dragItem.data);

        }else{
          // Put the Item back in the backpack
          dragItem.data.data.location = "backpack";
          dragItem.data.data.equipped = false;
          //console.log(dragData.data);
          await this.actor.updateEmbeddedEntity('OwnedItem', dragItem.data);
        }

        return false;
      }

    }
    return super._onDrop(event);


  }

  /**
   * Handle clickable rolls.
   * @param {Event} event   The originating click event
   * @private
   */
  _onStatRoll(event) {
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;

    if (dataset.roll) {

      var e = document.getElementById("rollmod");
      var strModifier = e.options[e.selectedIndex].value;
      var strLevelName = e.options[e.selectedIndex].innerHTML;
      //console.log("value: " + strLevelName);
      //console.log(dataset);

      var formula = dataset.roll ;
      let roll = new Roll(formula, this.actor.data.data);
      let label = dataset.label ? `Rolling   ${dataset.label} ` : '';

      if(dataset.stat){
        label = dataset.label ? `Rolling ` + strLevelName + ` ${dataset.label}`  : '';
      }

      var msg = label;
      var rollunder;

      roll.roll();
      var rollVal = roll.parts[0].rolls[0].roll;

      // Calculate what to roll under
      if(dataset.stat){
        rollunder = +this.actor.data.data.stats[dataset.stat] + +strModifier;
      }else{
        rollunder = +this.actor.data.data.stats[dataset.stat];
      }

      // Check if we are successful
      if(rollVal <= rollunder ){
        msg = msg + "<br>Success " + rollVal + " is under " + rollunder;
      }else{
        msg = msg + "<br>Failed  " + rollVal + " is over " + rollunder;
      }

      // Send the Roll message to the Chat
      roll.toMessage({
        speaker: ChatMessage.getSpeaker({ actor: this.actor }),
        flavor: msg
      });
    }
  }

  _onCheckRoll(event){
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;
    if (dataset.roll) {

      //let level = html.find("#skilllevel").value;

      let roll = new Roll(dataset.roll , this.actor.data.data);
      let label = dataset.label ? `Rolling ${dataset.label}` : '';
      roll.roll().toMessage({
        speaker: ChatMessage.getSpeaker({ actor: this.actor }),
        flavor: label
      });
    }
  }
  _onWeaponRoll(event) {
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;

    if (dataset.roll) {
      let stat = dataset.stat;
      var formula = dataset.roll + "+" + this.actor.data.data.stats[stat.toLowerCase()];
      let roll = new Roll(formula, this.actor.data.data);
      let label = dataset.label ? `Rolling ${dataset.label}` : '';

      roll.roll();
      let bonus = this.actor.data.data.stats[stat.toLowerCase()];
      let theRolls = [roll.parts[0].rolls[0].roll, roll.parts[0].rolls[1].roll, roll.parts[0].rolls[2].roll];
      theRolls.sort((a, b) => a - b);
      let roll1 = theRolls[0] ;
      let roll2 = theRolls[1] ;
      let roll3 = theRolls[2] ;


      let damageRoll = 0;


      var targetNum ="";
      var targetName = "";

      var targets = Array.from(game.user.targets);


      var damageMod = this._get_damageModified(dataset.weapontype);
      console.log("Dmg Mod: " + damageMod);


      if(targets.length > 0 ){
        var token = targets[0]
        var target = targets[0].actor.data;
        targetNum = target.data.armor;
        targetName =  target.name;

       // console.log(target);
        var bSuccessfulHit = false;
        var startingVigour = target.data.measures.vigour.value;


        var msg = "You missed " + targetName;
        if( (roll1 + roll2 + bonus ) >= targetNum ){
          damageRoll = roll3 + parseInt(damageMod);
          msg = "Success with a " + roll1 + " and " + roll2 + ", dealing " + damageRoll + " of damage to " + targetName;
          bSuccessfulHit = true;
        }else if( (roll1 + roll3 + bonus) >= targetNum){
          damageRoll = roll2 + parseInt(damageMod);
          msg = "Success with a " + roll1 + " and " + roll3 + ", dealing " + damageRoll + " of damage to " + targetName;
          bSuccessfulHit = true;
        }else if( (roll2 + roll3 + bonus) >= targetNum){
          damageRoll = roll1 +  parseInt(damageMod);
          msg = "Success with a " + roll2 + " and " + roll3 + ", dealing " + damageRoll + " of damage to " + targetName;
          bSuccessfulHit = true;
        }else{

        }



        /*
        if(bSuccessfulHit){
          token.actor.update({
            'data.measures.vigour.value': token.actor.data.data.measures.vigour.value - damageRoll
          });

          if(startingVigour - damageRoll <= 0){

              var combatants = game.combats.active.combatants;

              for(let i=0; i< combatants.length; i++){

                if(combatants[i].tokenId == token.data._id){
                  console.log(token);
                  console.log('Winner winnner chicken dinner');
                  console.log(combatants[i]);
                  //game.combats.entities[0].updateCombatant(combatants[i],{"combatants[i].defeated" : "true"});
                  var newCombatantData = {"_id": combatants[i]._id, "tokenId": combatants[i].tokenId, "defeated":true};

                  game.combats.active.updateEmbeddedEntity("Combatant", newCombatantData);

                }
              }
              //console.log(token.data._id);

          }
        }
  */

        //console.log("Target Num: " + targetNum);
        roll.toMessage({
          speaker: ChatMessage.getSpeaker({ actor: this.actor }),
          flavor: msg
        });

      }else{
        roll.toMessage({
          speaker: ChatMessage.getSpeaker({ actor: this.actor }),
          flavor: "Attack Roll add " + damageMod + " for damage"
        });
      }

    }else{
      ChatMessage.getSpeaker({content: "You must select a target to attack", actor: this.actor });
    }
  }

  _get_damageModified(weaponType){

      const isTwoHanded = this.actor.data.data.isTwoHanded;
      console.log("TwoHanded: " + isTwoHanded);
      if(weaponType === "hand" && isTwoHanded === true){
        return "+1";
      }

      if(weaponType === 'heavy'){
        return "+1";
      }

      if(weaponType === 'light'){
        return "-1";
      }

      if(weaponType === "long" && isTwoHanded){
        return "+1";
      }

      if(weaponType === "throwing"){

      }

      if(weaponType === "ranged"){

      }

      return "+0";
  }
  /* -------------------------------------------- */

  /** @override */
  setPosition(options={}) {
    const position = super.setPosition(options);
    const sheetBody = this.element.find(".sheet-body");
    const bodyHeight = position.height - 250;
    sheetBody.css("height", 500);
    return position;
  }

  /* -------------------------------------------- */

  /**
   * Listen for click events on an attribute control to modify the composition of attributes in the sheet
   * @param {MouseEvent} event    The originating left click event
   * @private
   */
  async _onClickAttributeControl(event) {
    event.preventDefault();
    const a = event.currentTarget;
    const action = a.dataset.action;
    const attrs = this.object.data.data.attributes;
    const form = this.form;

    // Add new attribute
    if ( action === "create" ) {
      const nk = Object.keys(attrs).length + 1;
      let newKey = document.createElement("div");
      newKey.innerHTML = `<input type="text" name="data.attributes.attr${nk}.key" value="attr${nk}"/>`;
      newKey = newKey.children[0];
      form.appendChild(newKey);
      await this._onSubmit(event);
    }

    // Remove existing attribute
    else if ( action === "delete" ) {
      const li = a.closest(".attribute");
      li.parentElement.removeChild(li);
      await this._onSubmit(event);
    }
  }

  /* -------------------------------------------- */

  /** @override */
  _updateObject(event, formData) {

    // Handle the free-form attributes list
    const formAttrs = expandObject(formData).data.attributes || {};
    const attributes = Object.values(formAttrs).reduce((obj, v) => {
      let k = v["key"].trim();
      if ( /[\s\.]/.test(k) )  return ui.notifications.error("Attribute keys may not contain spaces or periods");
      delete v["key"];
      obj[k] = v;
      return obj;
    }, {});
    
    // Remove attributes which are no longer used
    for ( let k of Object.keys(this.object.data.data.attributes) ) {
      if ( !attributes.hasOwnProperty(k) ) attributes[`-=${k}`] = null;
    }

    // Re-combine formData
    formData = Object.entries(formData).filter(e => !e[0].startsWith("data.attributes")).reduce((obj, e) => {
      obj[e[0]] = e[1];
      return obj;
    }, {_id: this.object._id, "data.attributes": attributes});
    
    // Update the Actor
    return this.object.update(formData);
  }

  _checkNull(items) {
    if (items && items.length) {
      return items;
    }
    return [];
  }
}
